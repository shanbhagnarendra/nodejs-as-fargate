const sum = require('./sum');

test('adds 1 + 2 to equal 3', () => {
    expect(sum.sum(1, 2)).toBe(3);
});

test ('adds -1 + 2 equal 1',()=>{
    expect (sum.sum(-1,2)).toBe(1);
})
test ('-1+4 equal 3',()=>{
   expect(sum.sum(-1,4)).toBe(3);
});

test('test add()',()=>{
   expect(sum.add(1,4)).toBe(5);
});

test('test add() for negetive numbers',()=>{
    expect(sum.add(-1,-4)).toBe(-5);
});
