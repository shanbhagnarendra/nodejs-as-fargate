const express = require("express");
const port = 3000;
const app = express();
const path = require("path");

//app.use(express.static(publicDir));

app.get("/", (req, res) => res.json({"message":"Welcome to Blue/Green deployment:116"}));

app.get("/health", (req, res) =>res.json({"status":200, message: "App is working","version":116}));


app.listen(port, () => {
    console.log(`Listening on port ${port}`);
});
